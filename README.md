# Audioramio documentation #

### Project ###

Goal of this project is to redesign this initial application: http://audioramio.com/ (Fabio's application)
So far it looks like [this](http://raluca-nicola.net/audioramio/).

### Ideas ###

* Use Leaflet for the map
* Try to implement the model-view-controller pattern. Learned about it in this course: https://www.udacity.com/course/viewer#!/c-ud989 
What came out is still something like spaghetti code grouped in model, view and controller objects :D
* Don't use any DOM Manipulation libraries. If needed, build a small utilities library.
* Load external code using module loaders (?)

### TO DO: ###

1. ~~Build a menu~~
2. ~~Create a layout (horizontal slider)~~
3. ~~Connect data with menu and map~~
4. Find a new way to declutter the map. There are several plugins for Leaflet: http://leafletjs.com/plugins.html#clusteringdecluttering. I tried the LayerGroup.Collision but it doesn't work when I try to filter the markers on the map.Detecting marker collision and setting visibility on markers accordingly is more interesting than clustering. The idea would be to either fix the collision issue on adding/removing markers or maybe a new interesting idea for clustering?  
5. Audio voice recording online
	* Figuring out the best way to record voice in a browser. Media Recorder is not supported by all browsers: https://developer.mozilla.org/en-US/docs/Web/API/MediaRecorder_API Storing user generated recordings also has the problem of having to upload the content on the server. Have no idea about security issues... Maybe a better idea would be to parse all these recordings of names from a website that allows free use of such audio files.
	* Implement the voice recording functionality through a form (user has to give in name, categories, record voice, coordinates etc.) in case we do build the user generated version. 
6. Style the popups to match the rest of the application (no iframes anymore).
7. Implement a Search functionality to search through the points of interest.
8. Find better categories for the menu? -> talk to Fabio about it.
9. Add text for the about section -> talk to Fabio about it.
10. Set up a database to store all the points? Not sure if necessary, audio files should better be in a file system anyway.