(function(){
	var w, h; 
	w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

	var Slider = function(container){
		var _this = this;
		var container = document.getElementById(container);
		var slider = container.children[0];
		this.slider = slider;
		this.container = container;

		var next = document.getElementById("next");
		next.addEventListener("click", function(){
			_this.move("right");
		});
		this.next = next;
		
		var prev = document.getElementById("prev");
		prev.addEventListener("click", function(){
			_this.move("left");
		});
		prev.style.display = 'none';
		this.prev = prev;


		window.addEventListener("resize", function(){
			w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
			h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
			_this.resize(w);
		})
	}

	Slider.prototype.move = function(direction){
      	this.slider.className = 'transition';
		if (direction === 'right') {
			this.prev.style.display = 'block';
			this.next.style.display = 'none';
			this.slider.style['right'] = w + 'px';
		}
		else {
			this.prev.style.display = 'none';
			this.next.style.display = 'block';
			this.slider.style['right'] = '0px';
		}
	}

	Slider.prototype.resize = function(w){
		this.slider.className = 'notransition';
		var right = this.slider.style['right'];
		this.slider.style['right'] = (right == '0px' || right == '') ? right : w + 'px';
	}
	window.Slider = Slider;
})(window, document)



	