(function(){
	var model = {
		loadGeoJSON: function(url, callback){
			var httpreq = new XMLHttpRequest();
			httpreq.open("GET", url, true);
			httpreq.send();
			httpreq.onreadystatechange = function(){
				if ((httpreq.readyState === XMLHttpRequest.DONE) && (httpreq.status === 200)){
					callback(httpreq.responseText);
				}
			}
		},
		buildCategories: function(){
			var categories = {};
			//parse through features and fill up category array.
			for (var i = 0; i<this.data.features.length; i++){
				var feature = this.data.features[i];
				var cat = feature.properties.category;
				for (prop in cat){
					if (prop in categories){
						if (categories[prop].indexOf(cat[prop]) < 0) {
							categories[prop].push(cat[prop]);
						}
					} else{
						categories[prop] = [cat[prop]];
					}
				}
			}
			return categories;
		},
		init: function(dataUrl){
			this.loadGeoJSON(dataUrl, function(data){
				this.data = JSON.parse(data);
				controller.dataLoaded();
			}.bind(this));
		}
	};

	var controller = {
		init: function(mapContainer,menuContainer,sliderContainer, dataUrl){
			views.map.init(mapContainer);
			views.slider.init(sliderContainer);
			this.menuContainer = menuContainer;
			model.init(dataUrl);
		},
		dataLoaded: function(){
			views.map.createPois(model.data);
			//build categories automatically from the data and add them to the menu
			var categories = model.buildCategories();
			views.menu.init(this.menuContainer, categories);
		},
		dataCategoryChanged: function(category){
			views.map.filterLayers(category);
			//call map.render
		}
	};

	var views = {
		map: {
			init: function(mapContainer){
				var mapOptions = {
					center: L.latLng(47.17477833929903, 7.75634765625),
					zoom:4,
					zoomControl:false
				}
				var map = L.map(document.getElementById(mapContainer), mapOptions);
				map.addControl(L.control.zoom({position:'bottomleft'}))
				this.basemap = map;
				L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
				id: 'mapbox.streets'}).addTo(map);
			},
			createPois: function(geojson){
				var options = {
					icon: L.icon({
						iconUrl: 'img/audiogrey25white.svg',
						iconSize: [25,25],
						iconAnchor: [7,25],
						shadowUrl: 'img/audio24shadow.png',
						shadowAnchor: [7,25]
					})
				};
				this.pois = [];
				for (var i=0; i<geojson.features.length; i++){
					var feature = geojson.features[i];
					var marker = L.marker([feature.geometry.coordinates[1],feature.geometry.coordinates[0]], options);
					marker.bindPopup(feature.properties.audio);
					marker.featureData = feature;
					this.pois.push(marker);
				}
				this.filterLayers();		
			},
			filterLayers: function(subcat){
				var cat = subcat || "kA";
				var selection = [];
				var map = this.basemap;
				this.pois.forEach(function(layer){
					var category = layer.featureData.properties.category;
					if ((cat === category[Object.keys(category)[0]]) || (cat === "kA"))  {
						selection.push(layer);
						if (!map.hasLayer(layer)){
							map.addLayer(layer);
						}
					}
					else {
						if (map.hasLayer(layer)){
							map.removeLayer(layer);
						}
					}
				});
				
				/*this.soundsLayer = null;
				var sL = L.layerGroup.collision();
				for (var i = 0; i<selection.length; i++){
					sL.addLayer(selection[i]);
				}
				map.addLayer(sL);
				this.soundsLayer = sL;
				*/
			}
		},
		menu: {
			init: function(menuContainer, categories){
				var menu = new Menu(menuContainer, categories);
				menu.onSelect(function(elem){
					//do something useful with the elem;
					controller.dataCategoryChanged(elem.innerHTML);
				})
				menu.onDeselect(function(){
					controller.dataCategoryChanged();
					//do something when user doesn't want any filters;
				})
			}	
		},
		slider:{
			init: function(container){
				var slider = new Slider(container);
			}
		}
	}

	controller.init("map", "menu-container", "slider-container", "data/testData.geojson");
})()