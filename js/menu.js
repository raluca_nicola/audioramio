(function(){
	var Menu = function(menuId, categories){

		var menuContainer = document.getElementById(menuId);
		var menu = document.createElement("div");
			menu.id = "menu";
			menuContainer.appendChild(menu);

		var menuList = document.createElement("ul");
			menu.appendChild(menuList);
		
		var submenu = document.createElement("div");
			submenu.id = "submenu";
			menuContainer.appendChild(submenu);

		var submenuList = document.createElement("ul");
			submenu.appendChild(submenuList);

		function deselect(cssClass){
			var currentSelection = document.getElementsByClassName(cssClass)[0];
			if (currentSelection) {
				currentSelection.classList.remove(cssClass);
				if (cssClass == "menu-selected") {
					var currentChildren = document.getElementsByClassName("submenu-" + currentSelection.classIndex);
					for(var i = 0; i < currentChildren.length; i++){
						var child = currentChildren[i];
						child.style.display = "none";
					}
				}
			}
		}

		var closeButton = document.createElement("button");
			closeButton.classList.add("close-submenu");
			closeButton.innerHTML = "x";
			submenu.appendChild(closeButton);
			closeButton.onDeselect = function(f){
				this.addEventListener("click", function(){
					submenu.style.display = "none";
					deselect("submenu-selected");
					deselect("menu-selected");
					f();
				})
			}

		var j = 0;

		for (var prop in categories){
			var menuCateg = document.createElement("li");
				menuCateg.innerHTML = prop;
				menuCateg.classIndex = j;
				menuList.appendChild(menuCateg);
			var subCategories = categories[prop]; 	
			if (subCategories.length != 0) {
				for (var i = 0; i < subCategories.length; i++){
					var submenuCateg = document.createElement("li");
					submenuCateg.classList.add("submenu-" + j);
					submenuCateg.innerHTML = subCategories[i];
					submenuCateg.style.display = "none";
					submenuList.appendChild(submenuCateg);
					submenuCateg.onSelect = function(f){
						this.addEventListener("click", function(){
							deselect("submenu-selected");
							this.classList.add("submenu-selected");
							f(this);
						});
					}
				}
			}

			menuCateg.addEventListener("click", (function(){
				return function(){
					submenu.style.display = "block";
					deselect("menu-selected");
					this.classList.add("menu-selected");
					var selectedChildren = document.getElementsByClassName("submenu-" + this.classIndex);	
					for(var i = 0; i < selectedChildren.length; i++){
						var child = selectedChildren[i];
						child.style.display = "inline";
					}
				}
			})());
			j++;
		}
		this.onSelect = function(f){
			var listItems = document.querySelectorAll("#submenu li");
			for (var id =0; id < listItems.length; id++){
				listItems[id].onSelect(f);
			}
		}
		this.onDeselect = function(f){
			closeButton.onDeselect(f);
		}
	}
	window.Menu = Menu;
})(window, document)



	